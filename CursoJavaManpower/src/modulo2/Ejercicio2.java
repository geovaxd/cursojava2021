package modulo2;

public class Ejercicio2 {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		byte bmin= -128;
		byte bmax= 127;
		short smin=-32768;
		short smax= 32767;
		int imin=-2147483648;
		int imax= 2147483647;
		long lmin=  -9223372036854775808l;
		long lmax= (long) 9223372036854775807l;
		
		System.out.println("tipo\tminimo\tmaximo");
		System.out.println(".....\t......\t......");
		System.out.println("nbyte\t"+bmin+"\t"+bmax);
		System.out.println("nshort\t"+smin+"\t"+smax);
		System.out.println("nint\t"+imin+"\t"+imax);
		System.out.println("nlong\t"+lmin+"\t"+lmax);
		
		//La formula general que he usado ha sido 2 elevado a los bits menos 1 para minimo y el resultado lo pongo en negativo, y para el positivo el resultado de la potencia le restamos 1 unidad  
	}

}


	


