package Modulo7;

import java.util.Objects;

public class Alumno extends Persona {
	private String iosfa;

	public Alumno() {
		super("Martinez", "Luis");
		iosfa = "General";
	}

	public Alumno(String apellido, String nombre, String iosfa){
		super(apellido, nombre);
		this.iosfa=iosfa;
	}

	public String getIosfa() {
		return iosfa;
	}

	public void setIosfa(String iosfa) {
		this.iosfa = iosfa;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Objects.hash(iosfa);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof Alumno))
			return false;
		Alumno other = (Alumno) obj;
		return Objects.equals(iosfa, other.iosfa);
	}
	@Override
	public String toString() {
		return super.toString() + ",iosfa=" + iosfa ;
	}
	
}
