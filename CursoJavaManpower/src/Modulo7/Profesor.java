package Modulo7;

public class Profesor extends Persona {
	private int legajo;


public Profesor() {
	super("Hernandez","Gabriel");
	legajo =1;
}


public Profesor(String apellido, String nombre, int legajo) {
	super(apellido,nombre);
	this.legajo=legajo;
}


public int getLegajo() {
	return legajo;
}


public void setLegajo(int legajo) {
	this.legajo = legajo;
}

public boolean equals(Object obj){		
	return obj instanceof Profesor 					&&
			super.equals(obj)  							&&
			legajo == ((Profesor)obj).getLegajo();		
}
public int hashCode(){
	return super.hashCode() + (int)(legajo);
}
public String toString(){
	return super.toString() + ",legajo=" + legajo;
}





}