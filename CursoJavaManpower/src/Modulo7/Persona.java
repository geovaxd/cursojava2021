package Modulo7;

import java.util.Objects;

public abstract class Persona {

	private String apellido;
	private String nombre;

	public Persona(){
		apellido = "Ruiz";
		nombre = "Horacio";
	}
	public Persona(String apellido, String nombre){
		this.apellido = apellido;
		this.nombre  = nombre;
	}
	
	
	
	
	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	
	@Override
	public int hashCode() {
		return Objects.hash(apellido, nombre);
	}
	@Override
	public boolean equals(Object obj){
		//establezco las reglas de negocio como yo quiero 
		
		boolean bln =false;
		if(obj instanceof Persona){
			//downcast			
			Persona cue = (Persona) obj;
			bln = 	this.apellido == cue.getApellido() &&
					this.nombre 	== cue.getNombre();
		}
		return bln;
	}
	
	public String toString(){
		return "nombre=" + nombre + ", apellido=" + apellido;
	}

	
}
