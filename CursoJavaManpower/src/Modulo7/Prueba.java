package Modulo7;

import java.util.Set;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import Modulo7.Persona;
import Modulo7.Alumno;
import Modulo7.Profesor;

public class Prueba {

	
	 Persona ProfesorVacio;
	 Persona ProfesorLleno;
	 Persona pp1Vacio;
	    //lisy y set son interfaces
	    List<Persona> lstCuentas; //pemite duplicados y es ordered (uno atras del otro
	    Set<Persona> setCuentas; //NO PERMITE suplicado y se guardan solo DIOS sabe;

	    @Before
		public void setUp() throws Exception {
			//upcast 
				
			ProfesorVacio = new Profesor();
			ProfesorLleno = new Profesor("Gutierrez","Hernan",4);
			pp1Vacio = new Profesor();
	
			lstCuentas = new ArrayList<Persona>();
			lstCuentas.add(new Profesor());
			lstCuentas.add(new Alumno());
			
			lstCuentas.add(new Profesor("Jimenez","Luis",5));
			lstCuentas.add(new Profesor("Diaz","Sett",6));
			lstCuentas.add(new Profesor("Jimenez","Luis",7));
			
			lstCuentas.add(new Alumno("Ray", "Rey", "Genuino"));
			lstCuentas.add(new Alumno("Rio", "Rayo", "Genuino"));
			lstCuentas.add(new Alumno("Leal", "Jrue", "Genuino"));
			
			
			setCuentas = new HashSet<Persona>();
			setCuentas.add(new Profesor());
			setCuentas.add(new Alumno());
			
			setCuentas.add(new Profesor("Rico","Damian",11));
			setCuentas.add(new Alumno("Griel", "Dame", "Paco"));
			
}
	    @After
		public void tearDown() throws Exception {
	    	ProfesorVacio = null;
	    	ProfesorLleno = null;
	    	pp1Vacio	= null;
			lstCuentas  = null;
			setCuentas  = null;
			
		}
		@Test
		public void listaEqualsContainsTRUE(){
			Persona cPrueba = new Profesor();
			assertTrue(lstCuentas.contains(cPrueba));
		}
		@Test
		public void listaEqualsContainsFALSE(){
			Persona cPrueba = new Profesor();
			cPrueba.setNombre("luis");
			assertFalse(lstCuentas.contains(cPrueba));
		}
		@Test
		public void testProfesorEqualsVERDADERO(){
			Persona Profesor = new Profesor();
			assertTrue(ProfesorVacio.equals(Profesor));
		}
		
		@Test
		public void testCajaDeAhorroEqualsFALSO(){
			Persona Profesor = new Profesor();
			Profesor.setNombre("luis");
			assertFalse(ProfesorVacio.equals(Profesor));
		}
	    
	    
}
