package DateUtil;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DateUtil {

	static int getAnio(Date fecha) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(fecha);
		return cal.get(Calendar.YEAR);
	}

	static int getMes(Date fecha) {

		Calendar cal = Calendar.getInstance();
		cal.setTime(fecha);
		return cal.get(Calendar.MONTH) + 1;
	}

	static int getDia(Date fecha) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(fecha);
		return cal.get(Calendar.DATE);

	}

	static boolean isFinDeSemana(Date fecha) {

		Calendar cal = Calendar.getInstance();
		cal.setTime(fecha);
		int dia = cal.get(Calendar.DAY_OF_WEEK);
		System.out.println(dia);

		if (dia == 0 || dia >= 5) {
			return true;
		} else {
			return false;
		}
	}

	static boolean isDiaDeSemana(Date fecha) {

		Calendar cal = Calendar.getInstance();
		cal.setTime(fecha);
		int dia = cal.get(Calendar.DAY_OF_WEEK);

		if (dia > 0 && dia < 5) {
			return true;
		} else {
			return false;
		}
	}

	static int getDiaDeSemana(Date fecha) {

		Calendar cal = Calendar.getInstance();
		cal.setTime(fecha);
		cal.get(Calendar.DAY_OF_WEEK);

		return cal.get(Calendar.DAY_OF_WEEK);
	}

	static Date asDate(String pattern, String fecha) {


		DateFormat df = new SimpleDateFormat(pattern);
		ZoneId defaultZoneId = ZoneId.systemDefault();
	
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern, Locale.ENGLISH);
		LocalDate localdate = LocalDate.parse(fecha, formatter);
		Date date = Date.from(localdate.atStartOfDay(defaultZoneId).toInstant());
		return date;
	}
	
	static Calendar asCalendar(String pattern, Date fecha) {
		return null;
		
		
	}

	static String asString(String pattern, Date fecha) {
		
		DateFormat df = new SimpleDateFormat(pattern);
		fecha = Calendar.getInstance().getTime();
		String hoy = df.format(fecha);
		
		return hoy;

	}

}
