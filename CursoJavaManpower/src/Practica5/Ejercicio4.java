package Practica5;

public class Ejercicio4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String palabra = "Esto es una prueba de la clase String";
		
		palabra=palabra.toLowerCase();
		int contadorvocal=0;
		int contadorconsonantes=0;
		for (int i = 0; i < palabra.length(); i++) {
			
			char letra=palabra.charAt(i);
			if(letra=='a' || letra =='e' || letra =='i' || letra =='o' || letra =='u') {
				contadorvocal++;
			}else {
				 contadorconsonantes++;
			}
			
		}
		
		System.out.println("Contador de vocales: "+contadorvocal);
		System.out.println("Contador de consonantes: "+contadorconsonantes);
	}
}
