package Modulo5;

import java.awt.Color;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.SwingConstants;


public class Pantalla_string {

	private JFrame frame;
	private JTextField textTexto;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Pantalla_string window = new Pantalla_string();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Pantalla_string() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Ingrese un texto");
		lblNewLabel.setBounds(54, 82, 113, 17);
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 11));
		frame.getContentPane().add(lblNewLabel);
		
		textTexto = new JTextField();
		textTexto.setBounds(177, 80, 127, 20);
		frame.getContentPane().add(textTexto);
		textTexto.setColumns(10);
		
		JLabel lblNewLabel_1 = new JLabel("MANEJO DE STRINGS");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblNewLabel_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_1.setBounds(111, 31, 236, 52);
		frame.getContentPane().add(lblNewLabel_1);
		
		JLabel lblMayuscula = new JLabel("Mayuscula");
		lblMayuscula.setBackground(Color.CYAN);
		lblMayuscula.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblMayuscula.setBounds(54, 118, 92, 14);
		frame.getContentPane().add(lblMayuscula);
		
		JLabel lblMinuscula = new JLabel("Minuscula");
		lblMinuscula.setAutoscrolls(true);
		lblMinuscula.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblMinuscula.setBounds(54, 151, 92, 14);
		frame.getContentPane().add(lblMinuscula);
		
		JLabel lblOX2 = new JLabel("0 x 2");
		lblOX2.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblOX2.setBounds(54, 187, 92, 14);
		frame.getContentPane().add(lblOX2);
		
		JLabel lblMayuscula_resultado = new JLabel("Resultado");
		lblMayuscula_resultado.setOpaque(true);
		lblMayuscula_resultado.setForeground(Color.BLACK);
		lblMayuscula_resultado.setFocusTraversalKeysEnabled(false);
		lblMayuscula_resultado.setBackground(Color.CYAN);
		lblMayuscula_resultado.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblMayuscula_resultado.setBounds(176, 111, 128, 27);
		frame.getContentPane().add(lblMayuscula_resultado);
		
		
		
		JLabel lblMinuscula_resultado = new JLabel("Resultado");
		lblMinuscula_resultado.setOpaque(true);
		lblMinuscula_resultado.setForeground(Color.BLACK);
		lblMinuscula_resultado.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblMinuscula_resultado.setFocusTraversalKeysEnabled(false);
		lblMinuscula_resultado.setBackground(Color.CYAN);
		lblMinuscula_resultado.setBounds(176, 144, 128, 27);
		frame.getContentPane().add(lblMinuscula_resultado);
		
		JLabel lblOX2_resultado = new JLabel("Resultado");
		lblOX2_resultado.setOpaque(true);
		lblOX2_resultado.setForeground(Color.BLACK);
		lblOX2_resultado.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblOX2_resultado.setFocusTraversalKeysEnabled(false);
		lblOX2_resultado.setBackground(Color.CYAN);
		lblOX2_resultado.setBounds(176, 180, 128, 27);
		frame.getContentPane().add(lblOX2_resultado);
		
		JButton btnCalcular = new JButton("Calcular");

		btnCalcular.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnCalcular.setBackground(Color.LIGHT_GRAY);
		btnCalcular.setBounds(139, 227, 165, 23);
		frame.getContentPane().add(btnCalcular);
		btnCalcular.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblMayuscula_resultado.setText(textTexto.getText().toUpperCase());
				lblMinuscula_resultado.setText(textTexto.getText().toLowerCase());
				lblOX2_resultado.setText(textTexto.getText().replace("O", "2"));
			}
		});
		
		
	}
}
