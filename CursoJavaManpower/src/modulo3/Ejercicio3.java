package modulo3;

import java.util.Scanner;

public class Ejercicio3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);

		System.out.println("Introduce un mes");

		String mes = sc.next();

		if (mes.equalsIgnoreCase("Enero") || mes.equalsIgnoreCase("Marzo") || mes.equalsIgnoreCase("Mayo")
				|| mes.equalsIgnoreCase("Julio") || mes.equalsIgnoreCase("Agosto") || mes.equalsIgnoreCase("Octubre")
				|| mes.equalsIgnoreCase("Diciembre")) {
			System.out.println("El mes "+mes+" tiene 31 dias");
		}
		if(mes.equalsIgnoreCase("Febrero")){
			System.out.println("El mes "+mes+" tiene 28 dias");
		}else {
			System.out.println("El mes "+mes+" tiene 30 dias");
		}
	}

}
