package modulo3;

import java.util.Scanner;

public class Ejercicio13 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
		Scanner sc = new Scanner(System.in);

		System.out.println("Introduce un mes");

		String mes = sc.next();
		
		switch (mes) {
		case "Enero", "Marzo", "Mayo","Julio","Agosto","Octubre","Diciembre": {
			System.out.println("El mes "+mes+" tiene 31 dias");
			break;
		}
		case "Febrero": {
			System.out.println("El mes "+mes+" tiene 28 dias");
			break;
		}
		default:
			System.out.println("El mes "+mes+" tiene 30 dias");
			break;
		}
	}

}
