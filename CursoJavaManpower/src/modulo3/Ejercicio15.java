package modulo3;

import java.util.Scanner;

public class Ejercicio15 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);

		System.out.println("Introduce una letra (a,b,c) para mostrar informacion");

		char letra = sc.next().charAt(0);

		switch (letra) {
		case 'a': {
			System.out.println("El coche tiene 4 ruedas y un motor");
			break;
		}
		case 'b': {
			System.out.println("El coche tiene 4 ruedas, motor, cierre centralizado y aire");
			break;
		}
		case 'c': {
			System.out.println("El coche tiene 4 ruedas y un motor, cierre centralizado, aire, airbag.");
			break;
		}
		default:
			System.out.println(("No ha introducido la letras mostradas"));
			break;
		}
	}

}
