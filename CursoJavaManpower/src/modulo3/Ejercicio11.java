package modulo3;

import java.util.Scanner;

public class Ejercicio11 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);

		System.out.println("Introduce una letra");

		char letra = sc.next().charAt(0);
		
		char a='a';
		char e='e';
		char i='i';
		char o='o';
		char u='u';
	
		if(letra == a || letra == e || letra == i || letra == o || letra == u ) {
			System.out.println("La letra introducida es una vocal");
		}else {
			System.out.println("La letra introducida no es una vocal");
		}
	
	}
}
	
