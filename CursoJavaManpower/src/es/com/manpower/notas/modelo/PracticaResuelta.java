package es.com.manpower.notas.modelo;

import java.util.Objects;

public class PracticaResuelta implements Model {
	private int codigo;
	private float nota;
	private String observaciones;
	
	public PracticaResuelta() {
		
	}

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public float getNota() {
		return nota;
	}

	public void setNota(float nota) {
		this.nota = nota;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	@Override
	public int hashCode() {
		return Objects.hash( nota, observaciones);
	}

	public boolean equals (Object obj) {
		return obj instanceof PracticaResuelta &&
				((PracticaResuelta)obj).getObservaciones().equals(observaciones);	
	}
	
	
	
	
}
