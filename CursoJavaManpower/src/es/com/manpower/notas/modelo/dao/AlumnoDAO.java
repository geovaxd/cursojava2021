package es.com.manpower.notas.modelo.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.mysql.cj.protocol.Resultset;
import com.mysql.cj.xdevapi.Result;

import es.com.manpower.notas.modelo.Alumno;
import es.com.manpower.notas.modelo.Model;
import es.com.manpower.notas.util.ConnectionManager;

public class AlumnoDAO implements DAO {
	private Connection con;

	@Override
	public void agregar(Model pMod) throws ClassNotFoundException, SQLException {
		// TODO Auto-generated method stub
		Alumno alumno = (Alumno) pMod;

		ConnectionManager.conectar();
		con = ConnectionManager.getConnection();
		StringBuilder sql = new StringBuilder(
				"insert into alumnos(ALU_NOMBRE,ALU_APELLIDO,ALU_ESTUDIOS,ALU_LINKGIT) VALUES (");
		sql.append("(?,?,?,?)");
		PreparedStatement pstm = con.prepareStatement(sql.toString());
		pstm.setString(1, alumno.getNombre());
		pstm.setString(1, alumno.getApellido());
		pstm.setString(1, alumno.getEstudios());
		pstm.setString(1, alumno.getLinkArepositorio());

		pstm.executeUpdate();

		ConnectionManager.desConectar();

	}

	@Override
	public void modificar(Model pMod) throws ClassNotFoundException, SQLException {
		// TODO Auto-generated method stub
		int alumnoactualizar = 1;
		ConnectionManager.conectar();
		con = ConnectionManager.getConnection();
		StringBuilder sql = new StringBuilder(
				"update from alumnos set ALU_NOMBRE='GUILLERMO' where ALU_ID='" + alumnoactualizar + "'");
		PreparedStatement pstm = con.prepareStatement(sql.toString());
		pstm.executeUpdate();
		ConnectionManager.desConectar();
	}

	@Override
	public void eliminar(Model pMod) throws ClassNotFoundException, SQLException {
		// TODO Auto-generated method stub

		ConnectionManager.conectar();
		int alumnoeliminar = 1;
		con = ConnectionManager.getConnection();
		StringBuilder sql = new StringBuilder("delete from alumnos where ALU_ID='" + alumnoeliminar + "'");
		PreparedStatement pstm = con.prepareStatement(sql.toString());
		pstm.executeUpdate();
		ConnectionManager.desConectar();
	}

	@Override
	public List<Model> leer(Model pMod) throws SQLException, ClassNotFoundException {
		Alumno alumno=(Alumno)pMod;
		List<Model> alumnos=new ArrayList<>();
		
		ConnectionManager.conectar();
		con = ConnectionManager.getConnection();
		
		StringBuilder sql = new StringBuilder("select ALU_ID,ALU_NOMBRE,ALU_APELLIDO,ALU_ESTUDIOS,ALU_LINKGIT");
		sql.append("from alumnos");
		
		if(alumno.getCodigo()>0)
		sql.append(" where alu_id=?");
		
		
		
		PreparedStatement pstm = con.prepareStatement(sql.toString());
		pstm.setInt(1, alumno.getCodigo());
		
		ResultSet rs= pstm.executeQuery();
		
		
		while(rs.next()) {
			alumnos.add(new Alumno (rs.getInt("ALU_ID"),
					rs.getString("ALU_NOMBRE"),
					rs.getString("ALU_APELLIDO"),
					rs.getString("ALU_ESTUDIOS"),
					rs.getString("ALU_LINKGIT")));
		}
		rs.close();
		ConnectionManager.desConectar();
		
		return alumnos;
		
		
		
	}
}
