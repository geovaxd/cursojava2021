package es.com.manpower.notas.util;



import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertFalse;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.junit.After;
import org.junit.Before;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ConnectionManagerTest {
	
	Connection con;
	
	@BeforeEach
	public void antes() throws ClassNotFoundException, SQLException {
		Class.forName("com.mysql.cj.jdbc.Driver");
		Connection conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306/manpower", "root","roots");
		ConnectionManager.conectar();
	}
	
	@After
	public void despues() throws SQLException {
		con=null;
		ConnectionManager.desConectar();
		
	}

	@Test
	void testConectar() {
		
		try {
			ConnectionManager.conectar();
			assertFalse(ConnectionManager.getConnection().isClosed());
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			assertTrue(false);
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	void testDesConectar() {
		try {
			ConnectionManager.desConectar();
			assertTrue(ConnectionManager.getConnection().isClosed());
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			assertTrue(false);
			e.printStackTrace();
		}
		
	}

	

	@Test
	void testGetConnection() throws SQLException {
		assertFalse(ConnectionManager.getConnection().isClosed());
	}

}
