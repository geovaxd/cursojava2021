package dao;

import java.sql.DriverManager;


import java.sql.Connection;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ConnectionTest {
	
	public ConnectionTest() {}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
		Class.forName("com.mysql.cj.jdbc.Driver");
		Connection conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306/manpower", "root","roots");
		Statement stm = conexion.createStatement();
		ResultSet rs= stm.executeQuery("select alu_id, alu_apellido, alu_nombre, alu_estudios, alu_linkgit from alumnos");
		while(rs.next()) {
			System.out.println("apellido ="+rs.getString("alu_apellido"));
			System.out.println(", nombre="+rs.getString("alu_nombre"));
		}
		rs.close();
		conexion.close();
	}catch (ClassNotFoundException e) {
		// TODO: handle exception
		e.printStackTrace();
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	}
}

