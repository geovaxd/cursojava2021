package Modulo8;

public class Rectangulo extends Figura {

	private float altura;
	private float base;

	public Rectangulo() {
		super();
		altura = 0;
		base = 0;
	}

	public Rectangulo(String nombre, float altura, float base) {
		super(nombre);
		this.altura = altura;
		this.base = base;
	}

	public float getAltura() {
		return altura;
	}

	public void setAltura(float altura) {
		this.altura = altura;
	}

	public float getBase() {
		return base;
	}

	public void setBase(float base) {
		this.base = base;
	}

	@Override
	public float calcularPerímetro() {

		float perimetro = altura * 2 + base * 2;

		return perimetro;
	}

	@Override
	public float calcularSuperficie() {

		float superficie = base * altura;

		if (superficie > Figura.getMaximaSuperficie()) {

			Figura.setMaximaSuperficie(superficie);

		}

		return superficie;
	}

	@Override
	public String getValores() {

		String valores = "Los valores de base: " + base + ", de altura: " + altura + ", el area: "
				+ calcularSuperficie() + ", y el perimetro: " + calcularPerímetro() + ".";

		return valores;
	}

	public int hashCode() {

		return (int) (getNombre().hashCode() + getMaximaSuperficie() + calcularPerímetro());

	}

	public boolean equals(Object odb) {

		return odb instanceof Rectangulo && super.equals(odb) && getNombre() == ((Rectangulo) odb).getNombre()
				&& getBase() == ((Rectangulo) odb).getBase() && getAltura() == ((Rectangulo) odb).getAltura();

	}

	public String toString() {

		return super.toString() + getValores();

	}

}
