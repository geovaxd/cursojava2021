package Modulo8;

public class Triangulo extends Figura {

	private float base, cateto1, cateto2, altura;

	public Triangulo() {
		super();
		base = 0;
		cateto1 = 0;
		cateto2 = 0;
		altura = 0;
	}

	public Triangulo(String nombre, float base, float cateto1, float cateto2, float altura) {
		super(nombre);
		this.base = base;
		this.cateto1 = cateto1;
		this.cateto2 = cateto2;
		this.altura = altura;

	}

	public float getBase() {
		return base;
	}

	public void setBase(float base) {
		this.base = base;
	}

	public float getCateto1() {
		return cateto1;
	}

	public void setCateto1(float cateto1) {
		this.cateto1 = cateto1;
	}

	public float getCateto2() {
		return cateto2;
	}

	public void setCateto2(float cateto2) {
		this.cateto2 = cateto2;
	}

	public float getAltura() {
		return altura;
	}

	public void setAltura(float altura) {
		this.altura = altura;
	}

	@Override
	public float calcularPerímetro() {

		float perimetro = base + cateto1 + cateto2;

		return perimetro;
	}

	@Override
	public float calcularSuperficie() {

		float superficie = (base * altura) / 2f;

		if (superficie > Figura.getMaximaSuperficie()) {

			Figura.setMaximaSuperficie(superficie);

		}

		return superficie;
	}

	@Override
	public String getValores() {

		String valores = "Los valores de base: " + base + ", de altura: " + altura + ", el cateto 1: " + cateto1
				+ ", el cateto2:" + cateto2 + ", el area: " + calcularSuperficie() + ", y el perimetro: "
				+ calcularPerímetro() + ".";

		return valores;
	}

	public int hashCode() {

		return (int) (getNombre().hashCode() + getMaximaSuperficie() + calcularPerímetro());

	}

	public boolean equals(Object odb) {

		return odb instanceof Triangulo && super.equals(odb) && getNombre() == ((Triangulo) odb).getNombre()
				&& getBase() == ((Triangulo) odb).getBase() && getAltura() == ((Triangulo) odb).getAltura();

	}

	public String toString() {

		return super.toString() + getValores() ;

	}

}
