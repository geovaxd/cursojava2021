package Modulo8;

public class Circulo extends Figura {
private float radio;
	
	public Circulo() {
		super();
		radio = 0;
	}
	
	public Circulo(String nombre , float radio) {
		super(nombre);
		this.radio = radio;
	}

	public float getRadio() {
		return radio;
	}

	public void setRadio(float radio) {
		this.radio = radio;
	}

	@Override
	public float calcularPerímetro() {
		
		float perimetro = 2 * (float)Math.PI * radio;
		
		return perimetro;
	}

	@Override
	public float calcularSuperficie() {
		
		float superficie = (float)Math.PI * radio * radio;
		
		if(superficie > Figura.getMaximaSuperficie()) {
			
			Figura.setMaximaSuperficie(superficie);
			
		}
		
		return superficie;
	}

	@Override
	public String getValores() {

		String valores = "Los valores de radio: " + radio + ", el area: " +calcularSuperficie()+", el perimetro: "+calcularPerímetro()+".";
		
		return valores;
	}
	
    public int hashCode() {
		
		return (int) (getNombre().hashCode() + getMaximaSuperficie() + calcularPerímetro());
		
	}
	
    public boolean equals(Object obj) {
		
		return obj instanceof Circulo
				&&super.equals(obj)
				&&getNombre() == ((Circulo)obj).getNombre()
				&&getRadio() == ((Circulo)obj).getRadio();
		
	}
    
    public String toString() {
    	
    	return super.toString() + getValores();
    	
    }
}
