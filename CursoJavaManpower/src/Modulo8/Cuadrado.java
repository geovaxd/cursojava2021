package Modulo8;

public class Cuadrado extends Figura {
	private float lado;

	public Cuadrado() {
		lado = 0;
	}

	public Cuadrado(String nombre, float lado) {
		super(nombre);
		this.lado = lado;
	}

	public float getLado() {
		return lado;
	}

	public void setLado(float lado) {
		this.lado = lado;
	}

	@Override
	public float calcularPerímetro() {

		float perimetro = lado * 4;

		return perimetro;
	}

	@Override
	public float calcularSuperficie() {

		float superficie = lado * lado;

		if (superficie > Figura.getMaximaSuperficie()) {

			Figura.setMaximaSuperficie(superficie);

		}

		return superficie;
	}
	
	@Override
	public String getValores() {

		String valores = "Los valores de lado: " + lado + ", el area: " +calcularSuperficie()+", el perimetro: "+calcularPerímetro()+".";
		
		return valores;
	}
	
	
	public int hashCode() {

		return (int) (getNombre().hashCode() + getMaximaSuperficie() + calcularPerímetro());
	}

	public boolean equals(Object odb) {

		return odb instanceof Cuadrado && super.equals(odb) && getNombre() == ((Cuadrado) odb).getNombre()
				&& getLado() == ((Cuadrado) odb).getLado();
	}

	public String toString() {

		return super.toString() + getValores() ;

	}


}
