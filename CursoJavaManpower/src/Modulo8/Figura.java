package Modulo8;

import java.util.Objects;

import Modulo7.Persona;

public abstract class Figura {

	private String nombre;
	private static float maximaSuperficie;

	public Figura() {

		nombre = "Figura";
		maximaSuperficie = 0;

	}

	public Figura(String nombre) {
		this.nombre = nombre;
	}

	public static float getMaximaSuperficie() {
		return maximaSuperficie;
	}

	public static void setMaximaSuperficie(float maximaSuperficie) {
		Figura.maximaSuperficie = maximaSuperficie;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public abstract float calcularPerímetro();

	public abstract float calcularSuperficie();

	public abstract String getValores();

	public int hashCode() {

		return Objects.hash(nombre, maximaSuperficie);

	}

	public boolean equals(Object obj) {

		boolean bln =false;
		if(obj instanceof Figura){
		return obj instanceof Figura && getNombre() == ((Figura) obj).getNombre();

	}
		return bln;
	}

	public String toString() {

		return "Nombre = " + nombre;

	}

}
