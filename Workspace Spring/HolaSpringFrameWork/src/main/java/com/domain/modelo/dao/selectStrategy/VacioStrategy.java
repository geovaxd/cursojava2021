package com.domain.modelo.dao.selectStrategy;

public class VacioStrategy extends SelectStrategy {

	public VacioStrategy() {
		this.isUltimo = false;
		this.tengoWhere =false;
	}

	@Override
	public String getCondicion() {		
		return "";
	}

	@Override
	public boolean isMe() {
		return isUltimo;
		
	}

}
