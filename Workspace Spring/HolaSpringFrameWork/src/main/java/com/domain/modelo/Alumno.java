package com.domain.modelo;


import java.util.List;
import java.util.Objects;

public class Alumno implements Model{
	public Alumno(int codigo, String nombre, String apellido, String estudios, String linkArepositorio) {
		super();
		this.codigo = codigo;
		this.nombre = nombre;
		this.apellido = apellido;
		this.estudios = estudios;
		this.linkArepositorio = linkArepositorio;
	}

	private int codigo;
	private String nombre;
	private String apellido;
	private String estudios;
	private String linkArepositorio;
	private List<PracticaResuelta> practicasResueltas;

	public Alumno(int codigo, String nombre, String apellido, String estudios, String linkArepositorio,
			List<PracticaResuelta> practicasResueltas) {
		super();
		this.codigo = codigo;
		this.nombre = nombre;
		this.apellido = apellido;
		this.estudios = estudios;
		this.linkArepositorio = linkArepositorio;
		this.practicasResueltas = practicasResueltas;
	}

	public Alumno() {

	}

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getEstudios() {
		return estudios;
	}

	public void setEstudios(String estudios) {
		this.estudios = estudios;
	}

	public String getLinkArepositorio() {
		return linkArepositorio;
	}

	public void setLinkArepositorio(String linkArepositorio) {
		this.linkArepositorio = linkArepositorio;
	}

	public List<PracticaResuelta> getPracticasResueltas() {
		return practicasResueltas;
	}

	public void setPracticasResueltas(List<PracticaResuelta> practicasResueltas) {
		this.practicasResueltas = practicasResueltas;
	}
	
	
	public boolean equals (Object obj) {
		return obj instanceof Alumno &&
				((Alumno)obj).getNombre().equals(nombre) &&
				((Alumno)obj).getApellido().equals(apellido);	
	}
	@Override
	public int hashCode() {
		return Objects.hash(apellido, nombre);
	}

	public String toString() {
		StringBuilder sb = new StringBuilder("codigo=");
		sb.append(this.codigo);
		sb.append("nombre=");
		sb.append(this.nombre);
		sb.append("apellido=");
		sb.append(this.apellido);
		sb.append("estudios=");
		sb.append(this.estudios);
		sb.append("link a repositorio=");
		sb.append(this.linkArepositorio);
		
		return sb.toString();
	}
	

	

}
