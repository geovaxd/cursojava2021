package com.domain.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionManager {
	private static Connection Connection;
	
	
	public static void  conectar() throws ClassNotFoundException, SQLException{
		
		Class.forName("com.mysql.cj.jdbc.Driver");
		Connection  = DriverManager.getConnection("jdbc:mysql://localhost:3306/manpower", "root","roots");
		
	}
	
	public static void desConectar() throws SQLException {
		Connection.close();
	}
	public static Connection getConnection() {
		return Connection;
	}
}
