package com.domain.controller;


import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import com.domain.modelo.dao.AlumnoDAO;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class IndexController {
	@RequestMapping("/home")
	public String goIndex() {
	return "index";
	}
	
	@RequestMapping("/")
	public String getPresentacion() {
		return "Presentacion";	}
	
	@RequestMapping("/listado")
	public String goListado(Model model) {
		
		List<com.domain.modelo.Model> alumnos =null;
		
		AlumnoDAO aluDao = new AlumnoDAO();
		
		
		
		try {
			aluDao.leer(null);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		model.addAttribute("titulo","Listado de alumnos");
		model.addAttribute("profesor","Gabriel Casas");
		model.addAttribute("alumnos", alumnos);
		
		return "Listado";
	}
}
