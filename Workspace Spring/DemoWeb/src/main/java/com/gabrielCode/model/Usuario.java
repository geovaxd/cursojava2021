package com.gabrielCode.model;

public class Usuario {
	private int codigo;
	private String clave;
	private String nombre;
	public Usuario(int codigo, String clave, String nombre) {
		super();
		this.codigo = codigo;
		this.clave = clave;
		this.nombre = nombre;
	}
	public int getCodigo() {
		return codigo;
	}
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	public String getClave() {
		return clave;
	}
	public void setClave(String clave) {
		this.clave = clave;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	
}
