package com.gabrielCode.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.gabrielCode.repository.IPersona;
import com.gabrielCode.service.IPersonaService;

public class PersonaRepoImpl1 implements IPersonaService{
	
	@Autowired
	@Qualifier("persona2")
	IPersona repo;
	@Override
	public void registrarHandler(String pNombre) {
		// TODO Auto-generated method stub
		repo.registrar(pNombre);
		
	}
	
}
