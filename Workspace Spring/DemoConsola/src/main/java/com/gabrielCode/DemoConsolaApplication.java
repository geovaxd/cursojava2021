package com.gabrielCode;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Service;

import com.gabrielCode.service.IPersonaService;
import com.gabrielCode.service.PersonaServiceImpl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.*;
import org.springframework.beans.factory.annotation.Autowired;

@SpringBootApplication
public class DemoConsolaApplication implements CommandLineRunner {
	private static Logger log = LoggerFactory.getLogger(DemoConsolaApplication.class);
	@Autowired
	private IPersonaService service;

	public static void main(String[] args) {
		SpringApplication.run(DemoConsolaApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		

		//service = new PersonaServiceImpl();

		service.registrarHandler("Gabriel");
	}
}
