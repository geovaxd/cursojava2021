package com.gabrielCode.SpringBoot;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.gabrielCode.beans.mundo;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ){
    	ApplicationContext appContext = new ClassPathXmlApplicationContext("com/gabrielCode/xml/beans.xml");
        mundo m = (mundo)appContext.getBean("mundo");
        System.out.println(m);
        ((ConfigurableApplicationContext)appContext).close();
    	
    }
}
